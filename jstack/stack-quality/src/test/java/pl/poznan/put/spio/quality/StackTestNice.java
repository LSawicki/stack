package pl.poznan.put.spio.quality;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StackTestNice {

    private Stack stack;

    @Before
    public void init() {
        stack = new Stack();
    }

    @Test
    public void testPush() {
        String string = "AAA";
        stack.push(string);
        assertEquals(1, stack.size());
    }

    @Test
    public void testReset() {
        String string = "AAA";
        stack.push(string);
        assertEquals(1, stack.size());
        stack.reset();
        assertEquals(0, stack.size());
    }

    @Test
    public void testPop() {
        String string = "AAA";
        stack.push(string);
        assertEquals(1, stack.size());
        Object result = stack.pop();
        assertEquals(0, stack.size());
        assertEquals("AAA", result);
    }

}

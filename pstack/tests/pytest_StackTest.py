# coding=utf-8
# importowanie modułów zależnych
import pytest

from stack.stack import Stack


# fixtury pytest pozwalają utworzyć bazowy stan dla testów
# w odróżnieniu od setUp i tearDown z biblioteki unittest, te konstrukcje muszą być wskazane ręcznie w testach
@pytest.fixture
def item():
    item = 'item1'
    return item


@pytest.fixture
def empty_stack():
    stack = Stack()
    return stack


@pytest.fixture
def one_element_stack(item):
    stack = Stack()
    stack.push(item)
    return stack


# wyznacznikiem testu jednostkowego jest prefix test_
# parametry empty_stack oraz item to utworzone wyżej konstrukcje fixture
def test_stack_push(empty_stack, item):
    # given
    stack = empty_stack

    # when
    stack.push(item)

    # then
    # elementy assert pozwalają tworzyć asercje/zapewnienia, po których spełnieniu możemy stwierdzić że test zakończył się bez błędu
    assert stack.size() == 1
    assert stack.get_collection() == [item]


def test_stack_pop(one_element_stack, item):
    # given
    stack = one_element_stack

    # when
    result = stack.pop()

    # then
    assert result == item
    assert stack.size() == 0


def test_get_stack(one_element_stack, item):
    # given
    stack = one_element_stack
    # given

    # when
    result = stack.get_collection()

    # then
    assert result == [item]

# Astack
Astack jest implementacją stosu w Angularze. Zawiera logikę biznesową w formie serwisu oraz prostą warstwę prezentacji, która z tego serwisu korzysta.
## Technologie
* Angular
* TypeScript
* SCSS
* Karma
* Jasmine
## Uwagi techniczne
### Preferowane IDE
VisualStudio Code lub IntellJ Ultimate
### Uruchamianie programu
`yarn start`
Następnie wystarczy wejść na http://localhost:4200/
### Uruchamianie testów
`yarn test`
